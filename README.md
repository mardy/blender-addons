# Blender addons

A repository of Blender addons written for my own use. Use them freely, and
feel welcome to contribute your own improvements. :-)

## Caption generator

File: [caption_generator.py](caption_generator.py)

Tutorial: https://youtu.be/wW1jVhHRU0I

This addon can be used to quickly generate captions and subtitles: lines are
read from a user-supplied text file, and `Font` objects are created by
duplicating the active object (which is expected to be a `Font` object itself).
In this way all generated font objects will have the proper settings, such as
position, font style, colour, bounding box, etc.

The visibility of the created object (as well as the initial active object)
will be set to a single frame: that is, the object showing the first line of
text will be visible only at frame 1, the one with the second line only at
frame 2, and so on; this makes it easy to generate a series of PNG images by
just rendering the scene as an animation: each caption will be written into a
PNG file. To make things simpler, the length of the scene will also be set to
the number of lines.

There is no menu item or panel to invoke the addon: just press the spacebar
while in the 3D view, type "caption" and you'll find the "Caption generator"
action.
