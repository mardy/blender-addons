bl_info = {
    "name": "Caption generator",
    "category": "object",
    "description": "Generate captions from text file and active font object",
    "author": "Alberto Mardegan",
    "version": (1, 0),
    "blender": (2, 78, 0),
    "tracker_url": "https://gitlab.com/mardy/blender-addons/issues",
    "support": "TESTING",
}

import bpy

class CaptionGenerator(bpy.types.Operator):
    """Generate captions from text file and active object"""
    bl_idname = "object.caption_generator"
    bl_label = "Caption generator"
    bl_options = {'REGISTER', 'UNDO'}

    filepath = bpy.props.StringProperty(name="Captions file", subtype="FILE_PATH", default="")

    def execute(self, context):
        self.generate_captions(context)
        return {'FINISHED'}

    def generate_captions(self, context):
        src = context.active_object
        path = bpy.path.abspath(self.filepath)
        with open(path) as lines:
            i = 0
            for line in lines:
                line = line.rstrip()
                if not line:
                    continue
                if i > 0:
                    obj = src.copy()
                    obj.data = src.data.copy()
                    obj.animation_data_clear()
                    context.scene.objects.link(obj)
                else:
                    obj = src
                obj.data.body = line
                
                self.set_visibility(obj, i + 1)
                i += 1
            context.scene.frame_end = i
            context.scene.update()
            
    def invoke(self, context, event):
        context.window_manager.fileselect_add(self) 
        return {'RUNNING_MODAL'}  
                
    def set_visibility(self, object, i):
        object.hide = True
        object.keyframe_insert(data_path="hide", frame=i-1)
        object.hide_render = True
        object.keyframe_insert(data_path="hide_render", frame=i-1)

        object.hide = False
        object.keyframe_insert(data_path="hide", frame=i)
        object.hide_render = False
        object.keyframe_insert(data_path="hide_render", frame=i)

        object.hide = True
        object.keyframe_insert(data_path="hide", frame=i+1)
        object.hide_render = True
        object.keyframe_insert(data_path="hide_render", frame=i+1)
    
def register():
    bpy.utils.register_class(CaptionGenerator)
    
def unregister():
    bpy.utils.unregister_class(CaptionGenerator)

if __name__ == "__main__":
    register()
        
